# 一步一步学ROS

![learn ROS](images/robot_ROS.jpg)

## 为什么学ROS？

 ROS的全称是Robot Operating System，即机器人操作系统。它提供一系列程序库和工具以帮助软件开发者创建机器人应用软件。 **ROS的核心就是提供了一套针对机器人应用的进程间通讯系统，使得不同功能的模块之间可以很方便的互联。** 如果把机器人的操纵比作一棵大树。而ROS充当的就是实现各个模块（树干，树叶）之间互通的脉络。让各个模块之间可以更方便，更快捷的互联互通。

不仅如此，经过ROS多年的发展，其内部早已完善了很多基础模块并且开源，比如：导航模块，机械臂控制模块，坐标系转换模块，各种SLAM模块等等。这将大大降低我们对于操控机器人的成本，不需要我们自己去造轮子，也不必从最底层的功能去实践。这对于初学者，无疑是十分友好的。

 最后，ROS的优点就是**针对机器人开发，ROS则提供了很好的可视化、模拟仿真和Debug的工具**。可视化的工具`RViz`，`Stage`和`Gazebo`就像通信工程师用的示波器，可以说没有`RViz`这种可视化工具，很多功能是无法调校出来的；ROS下的模拟仿真做到了和硬件的无缝衔接。

并且针对行业来说，ROS已经逐步在与各行各业融合。如：机器人控制器中的大佬人物——KEBA，他们的控制器已经支持ROS。美国NASA基于ROS开发的Robonaut 2已经在国际空间站里干活了。 百度Apollo无人车的底层是基于ROS开发的。以及ROS-I最近正准备和微软、宝马合作，开发一套自动化解决方案，等等。这都是在说明ROS的影响正在逐步的扩大。

ROS可以说是现在对于机器人的学习无法绕开的一个关卡。而ROS其内部涉及着大量的对于机器人运动学的理论知识，我们不想只是成为一个仅仅会调包的“调包侠”。所以对于ROS的学习也是需要去深入，尝试去理解那些已有模块的内部方法。从而提高自己的能力。使自己对机器人的理解更上一层楼。希望大家能够脚踏实地，认真学习。

## 如何学习ROS？

**本教程只针对ROS1，不讲ROS2**

Stage 0：基础知识(Linux,C++,Python)

- C++
  + 对于ROS，大部分模块如导航，建图等等是由C++来编写的，学好C++才能看懂其用法，更好理解其原理
  + 需要考虑如何评估自己的C++能力是否适应SLAM编程的需求，如果自己的能力不够，该如何学习？从哪里找资料去学习？
  + 如果C++能力比较弱，可以参考[《一步一步学编程》](https://gitee.com/pi-lab/learn_programming)学习C++编程、数据结构与算法、编程项目等练习
- Python
  - 对于ROS其也兼容Python代码，由于Python写法较为简便，所以在实现小工程使可以使用Python进行书写
  - 对于已有的Python工程，如深度学习工程，想要接入ROS通讯，这时学习Python就显得很重要
  - 如果Python能里较弱，可以参考[《廖雪峰老师的Python教程》](https://www.liaoxuefeng.com/wiki/1016959663602400),学习Python的基本知识
- Linux
  - 对于ROS，其使用的操作系统只要[Linux](https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/linux)，所以学好[Linux](https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/linux)尤为重要
  - 基本的命令，如何安装软件包，如何查找软件包等等
  - 如何在Linux下编译软件，如何使用[CMake](https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/cmake)
  - 如何使用Linux下面的IDE，例如QtCreator，KDevelop等等



**Stage 1: ROS基础学习**

- 安装ROS，去[官方安装网站](http://wiki.ros.org/cn/ROS/Installation)安装

  - 注意安装版本要与Ubuntu版本吻合
  - 若遇到`rosdep`指令因访问不了外网可暂时跳过这条指令
  - 使用指令时要注意指令的用处，不能盲目复制。
- 参照[ROS基本教程](http://wiki.ros.org/cn/ROS/Tutorials)学习
  - 在看教程之前也可以看Basic目录下的[README.md](Basics/README.md)对ROS通讯有一个初步的了解
  - 至少将初级教程学完
  - 看教程时，要明白指令的意义，不是盲目使用
  - 理解使用`Topic`和`Service`等
  - 熟练理解使用ROS一些图形化debug界面



**Stage 2: ROS中期学习**

- 加深对ROS的基本概念的理解
  - 需要看[官方基本概念](http://wiki.ros.org/cn/ROS/Concepts)与[命令行工具的使用](http://wiki.ros.org/cn/ROS/CommandLineTools)
  - 命令行工具不要求记得一字不差，只需有个印象，到时候即用即查
  - 基本概念主要了解"计算图"与"命名"
- 了解与实现一些简单的功能
  - 可以先看MidTerm文件下的[README.md](MidTerm/README.md)进行初步了解，一些教程写在这里
  - 要学会`roslaunch`的基本使用
  - 对一些简单模块：`odom`，`URDF`，`TF`理解并掌握
  - 要学着写一些简单的工程，比如
    - 利用代码控制基础教程中的小乌龟移动
    - 使用`URDF`知识创建个仿真小车，并控制其移动



**Stage 3: ROS各个模块学习**

这里就是真正进入机器人模块了，我这里把机器人分成六个模块来讲：上下位机通讯，建图，导航，机械臂，状态机，语音图像。这个学习没有先后顺序，可根据自己需要自行决定学习顺序

学习之前可以先看一下下方的[学习建议](##学习的建议)

- 上下位机通讯
  - 这里可以参看一下[Communication/README.md](Module/Communication/README.md)内含教程
  - 理解`ros_control`的运作过程
  - 理解`串口通讯`，`socket`通讯
  - 尝试使用`roscontrol`，以及串口通讯方式等
- 导航
  - 可以去看[官方教程](http://wiki.ros.org/cn/navigation),[代码](https://github.com/ros-planning/navigation)
  - 通过代码不难看出，`navigation`是一个多包的集成，想要深入了解每个包，可以在`http://wiki.ros.org/`后加入包的名称，如：[move_base](http://wiki.ros.org/move_base)
  - 理解导航需要订阅什么`topic`，和发布什么`topic`
  - 能够使用导航在仿真界面跑起来
  - 理解导航内部原理，尝试去读代码理解，move_base代码一定要读
- 建图(slam)

  - 这里推荐三个激光建图包,[gmapping](http://wiki.ros.org/gmapping) [hector_slam](http://wiki.ros.org/hector_slam) [cartographer](https://google-cartographer-ros.readthedocs.io/en/latest/)
  - 教程中有他的软件包安装方式，需要安装好，并能进行测试
  - 了解其参数使用
  - 了解其内部原理

- 机械臂控制
  - 先给出一个[理论说明](https://blog.csdn.net/dingjianfeng2014/article/details/71081801)
  - 这里给出[官方教程](http://moveit2_tutorials.picknik.ai.)，教程较长根据需求决定学习到哪
  - 这里推荐一个较好的机器人[蔽障算法](https://blog.csdn.net/weixin_44229927/article/details/111785436)
  - 至少要掌握机械臂控制的方法
  - 努力实现蔽障
- 状态机
  - 状态机是机器人的大脑，它是用来调用各个模块来达到想要的效果
  - [官方教程](http://wiki.ros.org/smach/Tutorials)
  - 最后要实现类似[行为树](https://www.guyuehome.com/5311)这样的效果
- 语音图像
  - 这里只需要看Module目录下的[rbx1_speech](Module/rbx1_vision)和[rbx1_vision](Module/rbx1_vision),这里面的例子就足够了
  - 学会使用其例子



## 学习的建议

如何学习这些模块呢，我觉得需要经历三个过程**用，读，写**

> 昨夜西风凋碧树，独上高楼，望尽天涯路

首先是用，将官方代码跑起来，让其能够实现功能。在使用的过程中一定会遇到错误，在解决这些错误的过程就多少会对这个模块有一个新的认识。 

> 衣带渐宽终不悔，为伊消得人憔悴

第二是读，尝试去读源码，理解其内部原理，通过读代码学习其代码的风格以及解决问题的逻辑思路，这也会对你的代码水平有较大的提高。      

> 众里寻他千百度，蓦然回首，那人却在，灯火阑珊处

最后就是写，在积累了这么多之后，就可以尝试去改进其源码的一些东西，来达到自己想要的一个更好的效果，甚至自己操刀重新写一个新的包。



**画重点**: 这里给大家提供以十分好用的[网站](https://docs.ros.org/en/)，这里你可以查到任何ros中使用的对象的方法。如，我想查找Costmap2D的方法,就点击[这](https://docs.ros.org/en/api/costmap_2d/html/namespacecostmap__2d.html)。对比这两个的网址，就可以找到你想要的各种类方法。

最后我也将`rbx1`这个包放入到`Module`目录里，大家可以去试着用和学习其简单样例，帮助学习。

