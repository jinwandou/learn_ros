# 浅谈ROS的一些基础功能

这里说一下ROS的一些基础模块以及一些进阶版的基础功能

## `roslaunch`

我们在基础教程中已经对`roslaunch`有了一个较为基础的认识，但是这是还不够的，我们还需要对他进行一个较为系统的学习一下。首先，对于其最基本的用途就是同时启动多个节点。但是不仅如此，当你的节点需要较多的参数时，`launch`文件会让其看起来更为舒适。`roslaunch`的功能还不止这些，请根据[roslaunch教程](https://blog.csdn.net/weixin_41995979/article/details/81784987)来进行学习。

## `odom`

odom全称odometry即里程计。我在这里说一下，主要是因为odom在ROS中有两个定义，希望大家不要弄混。

第一个定义是ROS的一个message，其内部主要包含当前机器人的位姿以及速度[官方定义](http://docs.ros.org/en/kinetic/api/nav_msgs/html/msg/Odometry.html)

第二个定义就是他是下文要讲的tf树上的常用的坐标系

## URDF文件

URDF文件主要是来描述机器人模型，可以手动创建一些简单为模型，对于一些大的模型一般都是通过solidsworks来进行导出的。其定义了关于机器人的各个link与joint，以及其坐标系的转换。一旦调用了URDF文件，就会自动调用TF变换(在下文)，来持续发布一些静态变换。可以参看一下这个[教程](https://blog.csdn.net/newbie_001/article/details/82594511)

## TF话题

先列一下[tf官方教程](http://wiki.ros.org/cn/tf/Tutorials),可以看完下述陈述，再去看官方教程，便于理解

### 简单介绍

TF(TransForm)他是用来表示坐标系变换的，就如同SLAM中，机器人运动变换一般用来表示同一机器人在不同时间的一个坐标变换，并一般由变换矩阵表示。而在一个机器人内部也需要很多的坐标变换，比如激光雷达与机器人身体之间坐标系变换(laser->base_link)等等，如下图所示。

<img src="../images/frames2.png" alt="frames2" style="zoom:50%;" />

可以看出一个普通机器人上有如此多的坐标系需要转换。而且不止是在机器本身上，还需有一个机器人与全局坐标系(map)的一个tf变换，来表示机器人的运动。

### 树状结构

而且由于有许多的坐标系要进行转换，TF一般将其优化成一个树状结构，可使用`rqt_tf_tree`来查看，如下图

<img src="../images/tf_frames.png" alt="tf_frames" style="zoom:67%;" />

这里要注意一个约定俗称的规则，一个健全的tf树都需要拥有**map,odom,base_footprint,base_link**之间的转换，至于原因，以后讲到导航再细说

### 如何发布

对于一些静态的转换，你可以使用命令行直接定义，也可以使用URDF文件。而对于那些动态的变换，则需要使用代码内部实现，这时候就需要看教程了。