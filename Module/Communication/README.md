# 上下位机通讯

如果想使用真正的机器人，就需要跟一些下位机的开发板打交道，让其知道机器人应该如何运动，并实时提供反馈，这才能实现真正意义的控制机器人。

这里我们主要是抽象成两个层面，ROS上的抽象与底层串口通讯的抽象

## ROS抽象

这里可以看这个理论[教程](https://www.cnblogs.com/lizhensheng/p/11253331.html)，他有张数据流图显示不出来我放到下面了，要想通过代码实现，则可看这个[教程](https://blog.csdn.net/qq_38288618/article/details/79005562)



![ros_control](../../images/ros_control.png)

## 串口通讯

这里一般的使用方法是使用c++的boost库中的asio的类方法，使用方法是将上下位机用串口线相连，然后使用类方法进行串口的读写。[教程](https://blog.csdn.net/weixin_30587927/article/details/95786013)

## 代码实例

朝闻道公司他们提出的开源项目OpenRE就是一个比较好的上下位机通讯的[实例](https://github.com/HANDS-FREE/handsfree/OpenRE)可以去了解一下

